<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([[
            'title' => 'a',
            'author' => 'ava',
            'created_at' => date('Y-m-d G:i:s'),
        ],
        [
            'title' => 'ba',
            'author' => 'bab',
            'created_at' => date('Y-m-d G:i:s'),
        ],
        [
            'title' => 'c',
            'author' => 'bab',
            'created_at' => date('Y-m-d G:i:s'),
        ],
        [
            'title' => 'd',
            'author' => 'caad',
            'created_at' => date('Y-m-d G:i:s'),
        ],
        [
            'title' => 'avar',
            'author' => 'caad',
            'created_at' => date('Y-m-d G:i:s'),
        ]
        ]
        );  
        
    }
}